# RANSlicing-iec61850

This project define the best line of some uses cases of the RAN slicing framework supporting IEC 61850

## Abstract

Fifth-generation (5G) and beyond systems are expected to accelerate the ongoing transformation of power systems towards the smart grid. However, the inherent heterogeneity in smart grid services and requirements pose significant challenges towards the definition of a unified network architecture. In this context, radio access network (RAN) slicing emerges as a key 5G enabler to ensure interoperable connectivity and service management in the smart grid. This article introduces a novel RAN slicing framework which leverages the potential of artificial intelligence (AI) to support IEC 61850 smart grid services. With the aid of deep reinforcement learning, efficient radio resource management for RAN slices is attained, while conforming to the stringent performance requirements of a smart grid self-healing use case. Our research outcomes advocate the adoption of emerging AI-native approaches for RAN slicing in beyond-5G systems, and lay the foundations for differentiated service provisioning in the smart grid.

## Content of the Repository

* Python scripts used to deploy the cellular Communication Network

* The Deep Reinforcement Learning Framework to enable the dynamic allocation of RAN slices based on an IEC 61850 protocol supporting a micro grid scenario.

* Raw output of the Spectral Efficiency.

## NOTE

The scripts will be released soon. (30.11.2022)
